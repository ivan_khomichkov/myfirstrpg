﻿using UnityEngine;

public class CameraRaycaster : MonoBehaviour
{
    //parametrs
    [Tooltip("We can change the prioratarised layer in inspector")]
    [SerializeField]Layer[] layerPriorities = 
    {
        Layer.Enemy,
        Layer.Walkable
    };
    [SerializeField] float distanceToBackground = 100f;

    //states
    Camera viewCamera;

    RaycastHit _hit;
    public RaycastHit Hit
    { get { return _hit; } }
    Layer _layer;
    public Layer LayerHit
    {
        get { return _layer; }
        set
        {
            if (_layer!=value)
            {
                _layer = value;
                OnLayerChanged(_layer);
            }
        }
    }



    private void Awake()
    {
        viewCamera = Camera.main;
    }


    public delegate void LayerChangedHandlers(Layer layer);


    public event LayerChangedHandlers LayerChanged;

    public void OnLayerChanged(Layer layer)
    {
        if (LayerChanged!=null)
        {
            LayerChanged(layer);
        }
    }

    void Update()
    {
        // Look for and return priority layer hit
        foreach (Layer layer in layerPriorities)
        {
            var hit = RaycastForLayer(layer);
            if (hit.HasValue)
            {
                _hit = hit.Value;
                LayerHit = layer;
                return;
            }
        }
        
        // Otherwise return background hit
        _hit.distance = distanceToBackground;
       
        LayerHit = Layer.RaycastEndStop;
    }

    RaycastHit? RaycastForLayer(Layer layer)
    {
        int layerMask = 1 << (int)layer; // See Unity docs for mask formation
        Ray ray = viewCamera.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit; // used as an out parameter
        bool hasHit = Physics.Raycast(ray, out hit, distanceToBackground, layerMask);
        if (hasHit)
        {
            return hit;
        }
        return null;
    }

   

    
}
