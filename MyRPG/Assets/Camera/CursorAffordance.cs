﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(CameraRaycaster))]
public class CursorAffordance : MonoBehaviour {

    [Header("Cursor Icons")]
    [SerializeField] Texture2D cursorWalkable = null;
    [SerializeField] Texture2D cursorEnemy = null;
    [SerializeField] Texture2D cursorUnknown = null;
    [Header("Cursor Offset")]
    [SerializeField] Vector2 curserOffset = new Vector2();


    CameraRaycaster cameraRaycaster;

    private void Awake()
    {
        cameraRaycaster = GetComponent<CameraRaycaster>();
        cameraRaycaster.LayerChanged += ChangeTheCursorIcon;
    }
    private void ChangeTheCursorIcon(Layer layer)
    {
        
        switch (layer)
        {
            case Layer.Walkable:
                Cursor.SetCursor(cursorWalkable, curserOffset, CursorMode.Auto);
                break;
            case Layer.Enemy:
                Cursor.SetCursor(cursorEnemy, curserOffset, CursorMode.Auto);
                break;
            case Layer.RaycastEndStop:
                Cursor.SetCursor(cursorUnknown, curserOffset, CursorMode.Auto);
                break;
            default:
                Debug.LogWarning("Something unexpected");
                break;
        }
    }
	
}
