using System;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.CrossPlatformInput;
[RequireComponent(typeof (ThirdPersonCharacter))]
public class PlayerMovement : MonoBehaviour
{
   
    //parametrs
    [SerializeField] float RadiusMoveStop = 0.2f;
    [SerializeField] float AttackMoveStop = 5f; 
    
    //states
    bool isIndirectMode = false; //TODO Think about making static
    CameraRaycaster cameraRaycaster;
    ThirdPersonCharacter m_Character;   // A reference to the ThirdPersonCharacter on the object
    Vector3 currentDistanation;
    private Vector3 m_Move;
    private Transform m_Cam;
    private Vector3 m_CamForward, clickPoint;

    private void Start()
    {
        cameraRaycaster = Camera.main.GetComponent<CameraRaycaster>();
        m_Character = GetComponent<ThirdPersonCharacter>();
        currentDistanation = transform.position;
        m_Cam = Camera.main.transform;
    }

    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {
        //TODO Let player change key for swithing
        SwitchOnOffDirectMovement(KeyCode.G);
        

        if (isIndirectMode)
        {
            ProcessDirectMovement();
        }
        else
        {
            ProcessIndirectMovement();
        }
        
    }

    private void SwitchOnOffDirectMovement(KeyCode key)
    {
        if (Input.GetKeyDown(key))
        {
            currentDistanation = transform.position; // clear the current clear target
            isIndirectMode = !isIndirectMode;
        }
    }

    private void ProcessDirectMovement()
    {
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");
       

        // calculate move direction to pass to character
        if (m_Cam != null)
        {
            // calculate camera relative direction to move:
            m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
            m_Move = v * m_CamForward + h * m_Cam.right;
        }
        else
        {
            // we use world-relative directions in the case of no main camera
            m_Move = v * Vector3.forward + h * Vector3.right;
        }
        m_Character.Move(m_Move, false, false);
    }

    private void ProcessIndirectMovement()
    {
        if (Input.GetMouseButton(0))
        {
            clickPoint = cameraRaycaster.Hit.point;
            switch (cameraRaycaster.LayerHit)
            {
                case Layer.Walkable:
                    currentDistanation = ShortendDistanation(clickPoint,RadiusMoveStop);
                    break;
                case Layer.Enemy:
                    currentDistanation = ShortendDistanation(clickPoint, AttackMoveStop);
                    break;
                case Layer.RaycastEndStop:
                    print("Out of map");
                    break;
                default:
                    break;
            }

        }

        m_Move = currentDistanation - transform.position;
        if (m_Move.magnitude >= 0)
        {
            m_Character.Move(m_Move, false, false);
        }
        else
        {
            m_Character.Move(Vector3.zero, false, false);
        }
    }

    private Vector3 ShortendDistanation(Vector3 clickPoint, float radiusMoveStop)
    {
        Vector3 reductionVector = (clickPoint - transform.position).normalized * radiusMoveStop;
        return clickPoint - reductionVector;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, currentDistanation);
        Gizmos.DrawSphere(currentDistanation,0.1f);
        Gizmos.DrawSphere(clickPoint,0.3f);

        //Draw attack sphere
        Gizmos.color = new Color(255,0,0,50);
        Gizmos.DrawWireSphere(transform.position,AttackMoveStop);
    }
}

