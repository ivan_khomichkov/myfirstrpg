﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinMe : MonoBehaviour {

	[SerializeField] float xRotationsPerMinute = 1f;
	[SerializeField] float yRotationsPerMinute = 1f;
	[SerializeField] float zRotationsPerMinute = 1f;
	
	void Update () {
        //xdegreePerFrame 
        float xDegreesPerFrame = 6f * xRotationsPerMinute * Time.deltaTime; 
        
        transform.RotateAround (transform.position, transform.right, xDegreesPerFrame);

		float yDegreesPerFrame = 6f * yRotationsPerMinute * Time.deltaTime;
        transform.RotateAround (transform.position, transform.up, yDegreesPerFrame);

        float zDegreesPerFrame = 6f * zRotationsPerMinute * Time.deltaTime;
        transform.RotateAround (transform.position, transform.forward, zDegreesPerFrame);
	}
}
